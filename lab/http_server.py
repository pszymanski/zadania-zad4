# -*- encoding: utf-8 -*-

import socket
import os
import logging
from daemon import runner
from email.utils import formatdate

class httpserver:
    
    def http_serve(self, server_socket):
        while True:
            # Czekanie na połączenie
            connection, client_address = server_socket.accept()
            # Odebranie żądania
            request = connection.recv(1024)
            log.info("REQUEST: "+request)
            if request:
                head = ""
                #print "Odebrano:"
                zadanie = request.split('\r\n')
                linijka = zadanie[0].split()
                URI = linijka[1]
                URI = "/home/p5/zadania-zad4/lab/web" + URI
                if not URI.endswith('/'):
                    if os.path.isfile(URI):
                        log.info("Requested file: "+URI)
                        rozbiteUri = URI.split('.')
                        dlugoscTablicy = len(rozbiteUri)
                        rozszerzenie = rozbiteUri[dlugoscTablicy-1]
                        if rozszerzenie == "html":
                            log.info("type: html")
                            html = open(URI).read()
                            head = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n"
                        elif rozszerzenie == "txt":
                            log.info("type: txt")
                            html = open(URI).read()
                            head = "HTTP/1.1 200 OK\r\nContent-Type: text/plain; charset=UTF-8\r\n\r\n"
                        elif rozszerzenie == "jpg" or rozszerzenie == "jpeg":
                            log.info("type: jpg")
                            html = open(URI, "rb").read()
                            head = "HTTP/1.1 200 OK\r\nContent-Type: image/jpeg\r\n\r\n"
                        elif rozszerzenie == "png":
                            log.info("type: png")
                            html = open(URI, "rb").read()
                            head = "HTTP/1.1 200 OK\r\nContent-Type: image/png\r\n\r\n"
                    else:
                        log.info("404: "+URI)
                        html = '<html><head></head><body><h1>404 Not Found</h1></body></html>'
                        head = "HTTP/1.1 404 Not Found\r\nContent-Type: text/html\r\n\r\n"
                else:
                    if os.path.isdir(URI):
                        log.info("Requested dir: "+URI)
                        html = '<html><head><title>Listing plików</title></head><body><h2>Lista plików w katalogu:</h2></br>'
                        for plik in os.listdir(URI):
                            if os.path.isdir(URI + plik):
                                html += '<a href="' + plik + '/">' + plik + '/</a><br>'
                            else:
                                html += '<a href="' + plik + '">' + plik + '</a><br>'
                                
                        html += '</body></html>'
                        head = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n"
                    else:
                        log.info("400: "+URI)
                        html = '<html><head></head><body><h1>400 Bad Request</h1></body></html>'
                        head = "HTTP/1.1 400 Bad Request\r\nContent-Type: text/html\r\n\r\n"
                    # Wysłanie zawartości strony
                log.info("RESPONSE: "+head)
                connection.sendall(head+html)
                
                # Zamknięcie połączenia
                connection.close()
                                
                
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path =  '/home/p5/httpdaemon.pid'
        self.pidfile_path =  '/home/pawel/tmp/httpdaemon.pid'
        self.pidfile_timeout = 5

    def run(self):
        # Tworzenie gniazda TCP/IP
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Ustawienie ponownego użycia tego samego gniazda
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        
        # Powiązanie gniazda z adresem
        server_address = ('', 31005)
        server_socket.bind(server_address)
        
        # Nasłuchiwanie przychodzących połączeń
        server_socket.listen(10)
        
        try:
            self.http_serve(server_socket)
            
        finally:
            server_socket.close()

#--------------------------------------------------------
log=logging.getLogger("http_server")
log.setLevel(logging.INFO)
handler = logging.FileHandler("demon.log")
log.addHandler(handler)
http=httpserver()

daemon_runner = runner.DaemonRunner(http)
daemon_runner.daemon_context.files_preserve = [handler.stream]
daemon_runner.do_action()
