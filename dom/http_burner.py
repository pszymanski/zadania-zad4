# -*- encoding: utf-8 -*-

from funkload.FunkLoadTestCase import FunkLoadTestCase
import socket, os, unittest, http_server

SERVER_HOST = os.environ.get('http_server', '194.29.175.240:31005')

class http_burner(FunkLoadTestCase):
    def test_dialog(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(("194.29.175.240", 31005))
        for i in range(0,100):
            sock.sendall("GET / HTTP/1.1\r\nHost: 194.29.175.240:31005\r\n")
        sock.close()
if __name__ == '__main__':
    unittest.main()
