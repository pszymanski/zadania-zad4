# -*- encoding: utf-8 -*-

import socket
import os
import logging
from email.utils import formatdate

def http_serve(server_socket, log):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        handle_client(connection,log)


def handle_client(connection,log):
        # Odebranie żądania
        request = connection.recv(1024)
        log.info("REQUEST: "+request)
        if request:
            head = ""
            #print "Odebrano:"
            zadanie = request.split('\r\n')
            linijka = zadanie[0].split()
            URI = linijka[1]
            URI = "/home/p5/zadania-zad4/dom/web" + URI
            if not URI.endswith('/'):
                if os.path.isfile(URI):
                    log.info("Requested file: "+URI)
                    rozbiteUri = URI.split('.')
                    dlugoscTablicy = len(rozbiteUri)
                    rozszerzenie = rozbiteUri[dlugoscTablicy-1]
                    if rozszerzenie == "html":
                        log.info("type: html")
                        html = open(URI).read()
                        head = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n"
                    elif rozszerzenie == "txt":
                        log.info("type: txt")
                        html = open(URI).read()
                        head = "HTTP/1.1 200 OK\r\nContent-Type: text/plain; charset=UTF-8\r\n\r\n"
                    elif rozszerzenie == "jpg" or rozszerzenie == "jpeg":
                        log.info("type: jpg")
                        html = open(URI, "rb").read()
                        head = "HTTP/1.1 200 OK\r\nContent-Type: image/jpeg\r\n\r\n"
                    elif rozszerzenie == "png":
                        log.info("type: png")
                        html = open(URI, "rb").read()
                        head = "HTTP/1.1 200 OK\r\nContent-Type: image/png\r\n\r\n"
                else:
                    log.info("404: "+URI)
                    html = '<html><head></head><body><h1>404 Not Found</h1></body></html>'
                    head = "HTTP/1.1 404 Not Found\r\nContent-Type: text/html\r\n\r\n"
            else:
                if os.path.isdir(URI):
                    log.info("Requested dir: "+URI)
                    html = '<html><head><title>Listing plików</title></head><body><h2>Lista plików w katalogu:</h2></br>'
                    for plik in os.listdir(URI):
                        if os.path.isdir(URI + plik):
                            html += '<a href="' + plik + '/">' + plik + '/</a><br>'
                        else:
                            html += '<a href="' + plik + '">' + plik + '</a><br>'
                            
                    html += '</body></html>'
                    head = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n"
                else:
                    log.info("400: "+URI)
                    html = '<html><head></head><body><h1>400 Bad Request</h1></body></html>'
                    head = "HTTP/1.1 400 Bad Request\r\nContent-Type: text/html\r\n\r\n"
                # Wysłanie zawartości strony
            log.info("RESPONSE: "+head)
            connection.sendall(head+html)
            
            # Zamknięcie połączenia
            connection.close()            
