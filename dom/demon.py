# -*- encoding: utf-8 -*-

import socket
import os
import logging
from daemon import runner
import http_server

class httpserver:
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path =  '/home/p5/httpdaemon.pid'
        self.pidfile_timeout = 5

    def run(self):
        # Tworzenie gniazda TCP/IP
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Ustawienie ponownego użycia tego samego gniazda
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        
        # Powiązanie gniazda z adresem
        server_address = ('', 31005)
        server_socket.bind(server_address)
        
        # Nasłuchiwanie przychodzących połączeń
        server_socket.listen(10)
        
        try:
            http_server.http_serve(server_socket, log)
            
        finally:
            server_socket.close()

#--------------------------------------------------------
log=logging.getLogger("http_server")
log.setLevel(logging.INFO)
handler = logging.FileHandler("demon.log")
log.addHandler(handler)

if __name__ == "__main__":
        http=httpserver()
        
        daemon_runner = runner.DaemonRunner(http)
        daemon_runner.daemon_context.files_preserve = [handler.stream]
        daemon_runner.do_action()
