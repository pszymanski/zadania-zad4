# -*- coding: utf-8 -*-

from SocketServer import ThreadingMixIn, TCPServer, BaseRequestHandler
import logging
import http_server
import socket


class MyHandler(BaseRequestHandler):

    def handle(self):
        http_server.handle_client(self.request, log)


class MyServer(ThreadingMixIn, TCPServer):
    allow_reuse_address = 1


if __name__ == '__main__':
    log=logging.getLogger("http_server")
    log.setLevel(logging.INFO)
    handler = logging.FileHandler("socket_server.log")
    log.addHandler(handler)

    server = MyServer(('', 31005), MyHandler)
    server.serve_forever()
